import 'package:flutter/material.dart';

import 'api/api.dart';
import 'model/topic.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'V2EX Hot Topic',
      theme: new ThemeData(
        primaryColor: Colors.white,
      ),
      home: new RandomWords(),
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  createState() => new RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final hotTopics = <Topic>[];
  final _saved = new Set<Topic>();
  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  initState(){
    requestHotTopic();
  }

  requestHotTopic() async{
    var stream =await getHotTopics();
    stream.listen((topics)=>hotTopics.add(topics));
  }

  Widget _buildSuggestions() {
    return new ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: hotTopics.length,
        itemBuilder: (context, i) {
          return _buildRow(hotTopics[i]);
        });
  }

  Widget _buildRow(Topic topic) {
    final alreadySaved = _saved.contains(topic);
    return new ListTile(
      title: new Text(
        topic.title,
        style: _biggerFont,
      ),
      trailing: new Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(topic);
          } else {
            _saved.add(topic);
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Startup Name Generator'),
        actions: <Widget>[
          new IconButton(icon: new Icon(Icons.list), onPressed: _pushSaved),
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (context) {
          final tiles = _saved.map(
            (topic) {
              return new ListTile(
                title: new Text(
                  topic.title,
                  style: _biggerFont,
                ),
              );
            },
          );
          final divided = ListTile
              .divideTiles(
                context: context,
                tiles: tiles,
              )
              .toList();
          return new Scaffold(
            appBar: new AppBar(
              title: new Text('Saved Suggestions'),
            ),
            body: new ListView(children: divided),
          );
        },
      ),
    );
  }
}
