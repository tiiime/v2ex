import 'node.dart';
import 'member.dart';

class Topic {
  int id;
  String title;
  String url;
  String content;
  String contentRendered;
  int replies;
  Member member;
  Node node;
  int created;
  int lastModified;
  int lastTouched;

  Topic.fromJson(Map map) {
    id = map['id'] as int;
    title = map['title'];
    url = map['url'];
    content = map['content'];
    contentRendered = map['content_rendered'];
    replies = map['replies'] as int;
    member = new Member.fromJson(map['member']);
    node = new Node.fromJson(map['node']);
    created = map['created'] as int;
    lastModified = map['last_modified'] as int;
    lastTouched = map['last_touched'] as int;
  }

  @override
  String toString() => 'Topic: $title - $node - $member';
}
