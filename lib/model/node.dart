class Node {
  int id;
  String name;
  String url;
  String title;
  String titleAlternative;
  int topics;
  int stars;
  String header;
  int footer;
  int created;
  String avatarMini;
  String avatarNormal;
  String avatarLarge;

  Node.fromJson(Map json) {
    id = json['id'] as int;
    name = json['name'];
    url = json['url'];
    title = json['title'];
    titleAlternative = json['title_alternative'];
    topics = json['topics'] as int;
    stars = json['stars'] as int;
    header = json['header'];
    footer = json['footer'] as int;
    created = json['created'] as int;
    avatarMini = json['avatar_mini'];
    avatarNormal = json['avatar_normal'];
    avatarLarge = json['avatar_large'];
  }

  @override
  String toString() => 'Node: $name';
}
