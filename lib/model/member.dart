class Member {
  int id;
  String username;
  String tagline;
  String avatarMini;
  String avatarNormal;
  String avatarLarge;

  Member.fromJson(Map json) {
    id = json['id'] as int;
    username = json['username'];
    tagline = json['tagline'];
    avatarMini = json['avatar_mini'];
    avatarNormal = json['avatar_normal'];
    avatarLarge = json['avatar_large'];
  }

  @override
  String toString() => 'Member: $username';
}
