import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/member.dart';
import '../model/node.dart';
import '../model/topic.dart';

main(List<String> args) {
  getNode("python");
  getUser(1);
}

Future<Stream<Topic>> getHotTopics() async {
  var url = "https://www.v2ex.com/api/topics/hot.json";
  var client = new http.Client();
  var streamTopics = await client.send(new http.Request('get', Uri.parse(url)));
  return streamTopics.stream
      .transform(UTF8.decoder)
      .transform(JSON.decoder)
      .expand((convert) => convert)
      .map((json) => new Topic.fromJson(json));
}

Future<Stream<Node>> getNode(String node) async {
  var url = "https://www.v2ex.com/api/nodes/show.json?name=${node}";
  var client = new http.Client();
  var streamTopics = await client.send(new http.Request('get', Uri.parse(url)));
  return streamTopics.stream
      .transform(UTF8.decoder)
      .transform(JSON.decoder)
      .map((json) => new Node.fromJson(json));
}

Future<Stream<Member>> getUser(int id) async {
  var url = "https://www.v2ex.com/api/members/show.json?id=$id";
  var client = new http.Client();
  var streamTopics = await client.send(new http.Request('get', Uri.parse(url)));
  return streamTopics.stream
      .transform(UTF8.decoder)
      .transform(JSON.decoder)
      .map((json) => new Member.fromJson(json));
}
